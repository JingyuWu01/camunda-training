package de.abas.mowu.twitter_qa;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.BpmnError;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;

@Named("createTweet")
public class CreateTweetDelegate implements JavaDelegate {

    private final Logger LOGGER = LoggerFactory.getLogger(CreateTweetDelegate.class.getName());

	@Override
	public void execute(DelegateExecution execution) throws Exception {
	    String content = (String)execution.getVariable("content");
	    LOGGER.info("Publishing tweet: " + content);
	    AccessToken accessToken = new AccessToken("220324559-jet1dkzhSOeDWdaclI48z5txJRFLCnLOK45qStvo", "B28Ze8VDucBdiE38aVQqTxOyPc7eHunxBVv7XgGim4say");
	    Twitter twitter = new TwitterFactory().getInstance();
	    twitter.setOAuthConsumer("lRhS80iIXXQtm6LM03awjvrvk", "gabtxwW8lnSL9yQUNdzAfgBOgIMSRqh7MegQs79GlKVWF36qLS");
	    twitter.setOAuthAccessToken(accessToken);  
	    try {
			twitter.updateStatus(content);
		} catch (TwitterException e) {
			throw new BpmnError("duplicate_message", "Error from twitter: " + e.getMessage());
		}
	}

}
