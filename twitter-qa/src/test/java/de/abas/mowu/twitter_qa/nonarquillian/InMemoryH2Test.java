package de.abas.mowu.twitter_qa.nonarquillian;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.init;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.processEngine;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.runtimeService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.taskService;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.withVariables;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.logging.LogFactory;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;

import de.abas.mowu.twitter_qa.LoggerDelegate;

/**
 * Test case starting an in-memory database-backed Process Engine.
 */
public class InMemoryH2Test {

  @ClassRule
  @Rule
  public static ProcessEngineRule rule = TestCoverageProcessEngineRuleBuilder.create().build();

  private static final String PROCESS_DEFINITION_KEY = "twitter-qa";

  static {
    LogFactory.useSlf4jLogging(); // MyBatis
  }

  @Before
  public void setup() {
    init(rule.getProcessEngine());
    Mocks.register("createTweet", new LoggerDelegate());
  }

  /**
   * Just tests if the process definition is deployable.
   */
  @Test
  @Deployment(resources = "process.bpmn")
  public void testParsingAndDeployment() {
    // nothing is done here, as we just want to check for exceptions during deployment
  }

  @Test
  @Deployment(resources = "process.bpmn")
  public void testHappyPath() {
	  //ProcessInstance processInstance = processEngine().getRuntimeService().startProcessInstanceByKey(PROCESS_DEFINITION_KEY);
	  
	  // Now: Drive the process by API and assert correct behavior by camunda-bpm-assert

		Map<String, Object> initVariables = new HashMap<String, Object>();
		initVariables.put("content", "A happy day");
		initVariables.put("approved", true);

		ProcessInstance processInstance = processEngine().getRuntimeService()
				.startProcessInstanceByKey(PROCESS_DEFINITION_KEY, initVariables);
		assertThat(processInstance).hasProcessDefinitionKey(PROCESS_DEFINITION_KEY);
		assertThat(processInstance).isStarted();
		assertThat(processInstance).isWaitingAt("usertask_reviewtweet");

		Task taskReviewTweet = taskService().createTaskQuery().taskCandidateGroup("management").list().get(0);
		assertThat(taskReviewTweet).hasName("Review Tweet");
		assertThat(taskReviewTweet).hasCandidateGroup("management");
		assertThat(taskReviewTweet).isNotAssigned();

		taskService().complete(taskReviewTweet.getId(), initVariables);
		assertThat(processInstance).isEnded().hasPassed("servicetask_publishtweet");
  }
  
	@Test
	@Deployment(resources = "process.bpmn")
	public void testSadPath() {
		ProcessInstanceWithVariables process = runtimeService().createProcessInstanceByKey(PROCESS_DEFINITION_KEY)
			.setVariables(withVariables("approved", false))
			.startAfterActivity("usertask_reviewtweet")
			.executeWithVariablesInReturn();
		
		assertThat(process).isEnded().hasPassed("scripttask_notify");
	}

}
